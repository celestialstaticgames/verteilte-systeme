using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Adventure.Server
{
    public class SocketServer
    {
        private Socket _currentClient;

        private readonly string _hostname;
        private readonly int _port;
        private readonly string _messageDelimiter;

        public SocketServer(string hostname, int port, string messageDelimiter)
        {
            _hostname = hostname;
            _port = port;
            _messageDelimiter = messageDelimiter;
        }

        public void Start()
        {
            var host = Dns.GetHostEntry(_hostname);
            var ipAddress = host.AddressList[0];
            var localEndPoint = new IPEndPoint(ipAddress, _port);

            Console.WriteLine("Starting server...");

            var listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            listener.Bind(localEndPoint);
            listener.Listen(10);

            Console.WriteLine($"Server listening on {_hostname}:{_port}.");

            Console.WriteLine("Listening for connection...");

            _currentClient = listener.Accept();
            _currentClient.ReceiveTimeout = -1;

            var messageLengthBytes = new byte[4];
            byte[] messageBytes = null;

            var bytesRec = _currentClient.Receive(messageLengthBytes);
            var messageLength = BitConverter.ToInt32(messageLengthBytes);

            if (bytesRec != 4)
            {
                Console.WriteLine($"Error receiving packet length. Expected 4, but got {bytesRec}.");
            }

            messageBytes = new byte[messageLength];
            bytesRec = _currentClient.Receive(messageBytes);
            var data = Encoding.ASCII.GetString(messageBytes, 0, bytesRec);

            Console.WriteLine($"Received message! {data}");

            SendMessage("Hello from server!");
        }

        public void SendMessage(string message)
        {
            var responseBuffer = new byte[1024];
            var msg = (byte[])Encoding.ASCII.GetBytes(message);
            var messageLengthBytes = BitConverter.GetBytes(message.Length);

            _currentClient.Send(messageLengthBytes);
            _currentClient.Send(msg);
        }
    }
}
