﻿using System;
using System.Net;
using System.Net.Sockets;

namespace Adventure.Server
{
    class Program
    {
        private readonly SocketServer _server;

        // Server-Routine
        // ##############
        static void Main(string[] args)
        {
            var server = new SocketServer("localhost", 8080, "<EOF/>");
            server.Start();
        }


    }
}
