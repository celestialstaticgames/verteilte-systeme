using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Adventure.Client
{
    public class SocketClient
    {
        private Socket _sender;

        private readonly string _hostname;
        private readonly int _port;

        public SocketClient(string hostname, int port)
        {
            _hostname = hostname;
            _port = port;
        }

        public void Start()
        {
            var host = Dns.GetHostEntry(_hostname);
            var ipAddress = host.AddressList[0];
            var remoteEP = new IPEndPoint(ipAddress, _port);
            _sender = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            _sender.Connect(remoteEP);
            _sender.ReceiveTimeout = -1;
            Console.WriteLine("Socket connected to {0}", _sender.RemoteEndPoint.ToString());

            SendMessage("This is a test!<EOF/>");

            while (true)
            {
                var messageLengthBytes = new byte[4];
                byte[] messageBytes = null;

                var bytesRec = _sender.Receive(messageLengthBytes);
                var messageLength = BitConverter.ToInt32(messageLengthBytes);

                if (bytesRec != 4)
                {
                    Console.WriteLine($"Error receiving packet length. Expected 4, but got {bytesRec}.");
                }

                messageBytes = new byte[messageLength];
                bytesRec = _sender.Receive(messageBytes);
                var data = Encoding.ASCII
                    .GetString(messageBytes, 0, bytesRec)
                    .Replace("<EOF/>", string.Empty);

                Console.WriteLine($"Received message: {data}");
            }
        }

        public void Shutdown()
        {
            _sender.Shutdown(SocketShutdown.Both);
            _sender.Close();
        }

        public void SendMessage(string message)
        {
            var responseBuffer = new byte[1024];
            var msg = (byte[])Encoding.ASCII.GetBytes(message);
            var messageLengthBytes = BitConverter.GetBytes(message.Length);

            _sender.Send(messageLengthBytes);
            _sender.Send(msg);
        }
    }
}
