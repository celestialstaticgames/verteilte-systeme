﻿using System;

namespace Adventure.Client
{
    class Program
    {
        // Client-Routine
        // ##############
        static void Main(string[] args)
        {
            var client = new SocketClient("localhost", 8080);
            client.Start();
            client.Shutdown();
        }
    }
}
